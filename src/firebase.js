// Import the functions you need from the SDKs you need
import { initializeApp } from "firebase/app";
import { getAnalytics } from "firebase/analytics";

const firebaseConfig = {
  apiKey: "AIzaSyBDeefR6jC87sMDjnQjNWKgvJ_U5y19oA8",
  authDomain: "relationship-counter-86c09.firebaseapp.com",
  projectId: "relationship-counter-86c09",
  storageBucket: "relationship-counter-86c09.appspot.com",
  messagingSenderId: "394964225412",
  appId: "1:394964225412:web:a2576a9f38ff0df29b21d1",
  measurementId: "G-DYQPTZC2WH"
};


const app = initializeApp(firebaseConfig);
const analytics = getAnalytics(app);