import React, { useEffect, useState } from 'react';
import { Link } from 'react-router-dom';
import * as Icon from 'react-bootstrap-icons';
import '../styles/Navbar.css';

export function Navbar() {
  const [couples, setCouples] = useState([]);

  useEffect(() => {
    fetch('http://localhost:3000/couples')
      .then(response => response.json())
      .then(data => setCouples(data))
      .catch(error => console.error(error));
  }, []);

  return (
    <>
      <div className="navbar-container">
        <h1 className="navbar-title">{couples.name}</h1>
        <Link to="/settings" className="navbar-button">
          <Icon.Sliders />
        </Link>
      </div>
    </>
  );
}
