import React, { useEffect, useState } from 'react';
import '../styles/MainScreen.css';
import MainImg from '/images/testcouple.jpg';
export function MainScreen(){

    const [couples, setCouples] = useState([]);

    useEffect(() => {
        fetch('http://localhost:3000/couples')
            .then(response => response.json())
            .then(data => setCouples(data))
            .catch(error => console.error(error));
    }, []);

    return (
        <>
                <div className="main-screen-container">
                <div className="image-container">
                    <img src={couples.image} alt="Test Couple" className="darkened-image" />
                    <div className="overlay-box">
                        <div className="overlay-content">
                        <h2>{new Date(couples.TogetherSince).getDate()}.</h2>
                        <h2>{new Date(couples.TogetherSince).toLocaleString('default', { month: 'short' })}</h2>
                        </div>
                    </div>
                </div>
            </div>
        </>
    );
}