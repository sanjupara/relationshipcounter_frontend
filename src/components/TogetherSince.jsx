import React, { useEffect, useState } from 'react';
import '../styles/TogetherSince.css';

export function TogetherSince() {
  const [couples, setCouples] = useState([]);

  useEffect(() => {
    fetch('http://localhost:3000/couples')
      .then(response => response.json())
      .then(data => setCouples(data))
      .catch(error => console.error(error));
  }, []);

  const calculateTimeDifference = startDate => {
    const currentDate = new Date();
    const togetherSinceDate = new Date(startDate);

    const timeDifference = currentDate - togetherSinceDate;

    const years = Math.floor(timeDifference / (1000 * 60 * 60 * 24 * 365.25));
    const months = Math.floor((timeDifference % (1000 * 60 * 60 * 24 * 365.25)) / (1000 * 60 * 60 * 24 * 30.44));
    const days = Math.floor((timeDifference % (1000 * 60 * 60 * 24 * 30.44)) / (1000 * 60 * 60 * 24));
    const weeks = Math.floor(days / 7);

    return { years, months, days, weeks, timeDifference };
  };

  const { years, months, days, weeks, timeDifference } = calculateTimeDifference(couples.TogetherSince || '2022-12-17');

  const totalMonths = years * 12 + months;
  const totalWeeks = Math.floor(timeDifference / (1000 * 60 * 60 * 24 * 7));
  const totalDays = Math.floor(timeDifference / (1000 * 60 * 60 * 24));

  return (
    <>
      <h2>Together since</h2>

      <div className="time-section-container">
        <div className="time-section">
          <h3>{years}</h3>
          <h4>years</h4>
        </div>

        <div className="time-section">
          <h3>{months}</h3>
          <h4>months</h4>
        </div>

        <div className="time-section">
          <h3>{days}</h3>
          <h4>days</h4>
        </div>
      </div>

      <div className="line"></div>

      <h2>That equals</h2>

      <div className="time-section-container">
        <div className="time-section">
          <h3>{totalMonths}</h3>
          <h4>total months</h4>
        </div>

        <div className="time-section">
          <h3>{totalWeeks}</h3>
          <h4>total weeks</h4>
        </div>

        <div className="time-section">
          <h3>{totalDays}</h3>
          <h4>total days</h4>
        </div>
      </div>
    </>
  );
}
