import React, { useEffect, useState } from 'react';
import { Navbar } from '../components/Navbar';
import { MainScreen } from '../components/MainScreen';
import { TogetherSince } from '../components/TogetherSince';


export function Startpage() {


    return (
        <>
            <Navbar/>
            <MainScreen/>
            <TogetherSince/>
        </>
    );
}