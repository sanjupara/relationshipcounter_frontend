import React, { useEffect, useState } from 'react';
import { Link } from 'react-router-dom';
import { useNavigate } from 'react-router-dom';
import * as Icon from 'react-bootstrap-icons';
import Calendar from 'react-calendar';
import 'react-calendar/dist/Calendar.css';
import '../styles/Settings.css';

export function Settings() {
  const [couples, setCouples] = useState({
    name: '',
    TogetherSince: new Date(),
    image: '',
  });

  const [showCalendar, setShowCalendar] = useState(false);
  const navigate = useNavigate();

  const handlerRedirect = () => {
    navigate('/');
  }

  useEffect(() => {
    fetch('http://localhost:3000/couples')
      .then(response => response.json())
      .then(data => setCouples(data))
      .catch(error => console.error(error));
  }, []);

  const handleChangeImage = () => {
    console.log('Change Image');
    // Add logic to open file explorer and handle image change
  };

  const handleSave = () => {
    fetch('http://localhost:3000/couples/update', {
      method: 'PUT',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(couples),
    })
      .then(response => response.json())
      .then(updatedData => {
        console.log('Data updated:', updatedData);
        setCouples(updatedData);
        handlerRedirect();
      })
      .catch(error => console.error('Error updating data:', error));
  };

  const handleNameChange = event => {
    setCouples(prevCouples => ({
      ...prevCouples,
      name: event.target.value,
    }));
  };

  const handleTogetherSinceChange = date => {
    setCouples(prevCouples => ({
      ...prevCouples,
      TogetherSince: date,
    }));
    setShowCalendar(false);
  };

  const toggleCalendar = () => {
    setShowCalendar(!showCalendar);
  };

  return (
    <>
      <div className="navbar-container">
        <Link to="/" className="navbar-button">
          <Icon.Arrow90degLeft />
        </Link>
        <h1 className="navbar-title">Settings</h1>
      </div>

      <div className="image-container">
        <img
          src={couples.image}
          alt="Test Couple"
          className="centered-image"
          onClick={handleChangeImage}
        />
      </div>

      <h3>Name:</h3>
      <input type="text" value={couples.name} onChange={handleNameChange} />
      <br />
      <h3>Together since:</h3>
      <div className="date-picker-container">
        <input
          type="text"
          value={couples.TogetherSince instanceof Date ? couples.TogetherSince.toDateString() : ''}
          onClick={toggleCalendar}
          readOnly
        />
        {showCalendar && (
          <Calendar
            onChange={handleTogetherSinceChange}
            value={couples.TogetherSince}
          />
        )}
      </div>
      <br />
      <button onClick={handleSave}>Save</button>
    </>
  );
}
