import './App.css'
import { Navbar } from './components/Navbar'
import { MainScreen } from './components/MainScreen';
import { TogetherSince } from './components/TogetherSince';
import { BrowserRouter, Routes, Route } from 'react-router-dom'
import { Startpage } from './pages/Startpage';
import { Settings } from './pages/Settings';


function App() {

  return (
    <>
    <BrowserRouter>
        <Routes>
            <Route path="/" element={<Startpage />} />
            <Route path="/settings" element={<Settings />} />
            <Route path="*" element={<h1>404: Not Found</h1>} />
        </Routes>
    </BrowserRouter> 

      
    </>
  )
}

export default App
